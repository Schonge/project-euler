package com.jconnolly.projeuler.problems;

/**
 * If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
 * If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
 * 
 * NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. 
 * The use of "and" when writing out numbers is in compliance with British usage.
 * 
 * @author John Connolly 
 * @version v1.0
 */

public class Problem17
{
	public static int and = 3 ; // The number of letters in the word and
	public static int thousand = 8 ; // The number of letters in the word thousand
	public static int hundred = 7 ; // The number of letters in the word hundred
	public static String[] units = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"} ;
	public static String[] special = {"eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"} ;
	public static String[] tens = {"ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"} ;
    
	public static void main(String[] args)
    {
    	int totalCount = 0 ;
        for(int i = 1 ; i <= 1000 ; i++)
        {
        	if(i % 1000 == 0)
        	{
        		totalCount += units[0].length() + thousand ;
        	} else if(i % 100 == 0) {
        		totalCount += getHundreds(i) ;
        	} else if(i % 10 == 0) {
        		totalCount += getTens(i) ;
        	}  else if(i > 10 && i < 20) {
        		totalCount += getSpecial(i) ;
        	} else if(i < 10 && i != 0) {
        		totalCount += getUnits(i) ;
        	}
        }
        
        System.out.println(totalCount) ;
    }
	
	public static int getUnits(int i)
	{
		int count = units[i - 1].length() ;
		return count;
	}
	
	public static int getTens(int i)
	{
		int count = tens[(i/10) - 1].length();
		return count;
	}
	
	public static int getSpecial(int i)
	{
		int count = special[(i/10) - 1].length();
		return count;
	}
	
	public static int getHundreds(int i)
	{
		int count = getUnits(i) + hundred ;
		return count ;
	}
}