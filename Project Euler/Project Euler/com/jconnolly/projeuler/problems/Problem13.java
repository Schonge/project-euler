package com.jconnolly.projeuler.problems;

/**
 * Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.
 * 
 * @author John Connolly 
 * @version v1.0
 */

import java.io.InputStream;
import java.util.Scanner ;
import java.math.BigInteger ;

public class Problem13
{
    public static void main(String[] args) throws Exception
    {
        // Create File to read in
    	ClassLoader loader = Thread.currentThread().getContextClassLoader() ;
    	InputStream file = loader.getResourceAsStream("50DigitNumbers.txt") ;
        // Read in file
        Scanner scan = new Scanner(file) ;
        
        // Set sum to 0 using BigInteger because numbers are so large BigInteger has to be used
        BigInteger sum = BigInteger.ZERO ;
        String str = "" ;
        // Scan through all the lines in the file
        while(scan.hasNext())
        {
            // Scan each line individually
            String s = scan.nextLine() ;
            // Convert the line to BigInteger number
            BigInteger num = new BigInteger(s) ;
            // Add the BigInteger number to sum
            sum = sum.add(num) ;
            // Change the sum back to a String
            str = sum.toString() ;            
        }
        /*
         * Once all the 50 digit numbers are added together and converted back to a String we get the
         * first 10 Digits of the sum by using substring()
         */ 
        System.out.print(str.substring(0, 10)) ;
        scan.close();
    }
}

// Correct Answer
