package com.jconnolly.projeuler.problems;

/**
 * 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
 * What is the sum of the digits of the number 2^1000?
 * 
 * @author John Connolly 
 * @version v1.0
 */

// Importing Class for BigInteger
import java.math.BigInteger ;

public class Problem16
{
    public static void main(String[] args)
    {
        // Have to use Big Integer as number becomes too big causing it to be a decimal number with an E in it
        BigInteger answer = BigInteger.valueOf(2) ;
        // Get 2^1000
        answer = answer.pow(1000) ;
        // Call sumOfDigits method and convert answer to String
        // This allows us to be able to tgrab each digit individualy
        System.out.println(sumOfDigits(answer.toString())) ;
    }
    
    // Method that adds all digits of the answer of 2^1000 together
    public static int sumOfDigits(String number)
    {
        int sum = 0 ;
        // Loops through all the digits
        for(int i = 0 ; i < number.length() ; i++)
        {
            // Takes each digit as a character
            Character c = new Character(number.charAt(i)) ;
            // Converts the character to a String
            String character = c.toString() ;
            // Converts the String back to an Integer
            int j = Integer.parseInt(character) ;
            sum += j ;
        }
        return sum ;
    }
}

// Correct Answer
