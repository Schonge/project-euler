package com.jconnolly.projeuler.problems;

/**
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 * 
 * @author John Connolly 
 * @version v1.0
 */

public class Problem5
{
    public static void main(String[] args)
    {
        int number = 21 ;
        for(number = 21; ; number++)
        {
            // Checks if number can be divided by all of the numbers from 2 - 20
            // as every number is divisible by 0 and 1 anyway
            if(number % 2 == 0
                && number % 3 == 0
                && number % 4 == 0
                && number % 5 == 0
                && number % 6 == 0
                && number % 7 == 0
                && number % 8 == 0
                && number % 9 == 0
                && number % 10 == 0
                && number % 11 == 0
                && number % 12 == 0
                && number % 13 == 0
                && number % 14 == 0
                && number % 15 == 0
                && number % 16 == 0
                && number % 17 == 0
                && number % 18 == 0
                && number % 19 == 0
                && number % 20 == 0)
                {
                    System.out.print(number) ;
                    break ;
                }
        }
    }
}

// Correct Answer - Not the best looking answer but a correct one

