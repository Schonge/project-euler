package com.jconnolly.projeuler.problems;

/**
 * Find the difference between the sum of the squares of the first one hundred natural numbers
 * and the square of the sum.
 * 
 * @author John Connolly 
 * @version v1.0
 */

public class Problem6
{
    public static void main(String[] args)
    {
        System.out.print(sumOfNumsSquared() - sumOfSquares()) ;
    }
    
    // Squares all the numbers from 1 - 100 and adds them together
    public static int sumOfSquares()
    {
        int sum = 0 ;
        for(int i = 1 ; i <= 100 ; i++)
        {
            sum += (i * i) ;
        }
        return sum ;
    }
    
    // Adds all the numbers together and gets the square of the total 
    public static int sumOfNumsSquared()
    {
        int sum = 0 ;
        for(int i = 0 ; i <= 100 ; i++)
        {
            sum += i ;
        }
        return sum * sum ;
    }
}

// Correct Answer
