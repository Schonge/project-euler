package com.jconnolly.projeuler.problems;

/**
 * The Fibonacci sequence is defined by the recurrence relation:
 * 
 * Fn = Fn-1 + Fn-2, where F1 = 1 and F2 = 1.
 * Hence the first 12 terms will be:
 * 
 * F1 = 1
 * F2 = 1
 * F3 = 2
 * F4 = 3
 * F5 = 5
 * F6 = 8
 * F7 = 13
 * F8 = 21
 * F9 = 34
 * F10 = 55
 * F11 = 89
 * F12 = 144
 * 
 * The 12th term, F12, is the first term to contain three digits.
 * 
 * What is the first term in the Fibonacci sequence to contain 1000 digits?
 * 
 * @author JoConnolly
 * @version v1.0
 *
 */

import java.math.BigInteger ;

public class Problem25 {
	
	public static void main(String[] args)
	{
		Problem25 p25 = new Problem25() ;
		p25.execute() ;
	}
	
	public void execute()
	{
		BigInteger a = BigInteger.valueOf(0) ;
		BigInteger b = BigInteger.valueOf(1) ;
		BigInteger c = BigInteger.valueOf(0) ;
		// First 2 terms are included because c starts as the third term
		// therefore can be counted already
		int term = 2 ;
		while(c.toString().length() <= 1000)
		{
			c = a.add(b) ;
			a = b ;
			b = c ;
			if(c.toString().length() == 1000)
			{
				break ;
			}
			term++ ;
		}
		System.out.println(term) ;
		
	}
}

// Correct Answer
