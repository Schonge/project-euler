package com.jconnolly.projeuler.problems;

/**
 * What is the 10001st prime number?
 * 
 * @author John Connolly 
 * @version v1.0
 */

public class Problem7
{
    public static void main(String[] args)
    {
        long number = 1 ;
        int numOfPrimes = 0 ;
        
        while(numOfPrimes < 10001)
        {
            number++ ;
            if(isPrime(number))
            {
                numOfPrimes++ ;
            }
        }
        System.out.println(number) ;        
    }
    
    // Method to check if number is prime
    public static boolean isPrime(long n) {
        // Both 0 and 1 are not Prime Numbers
        if (n < 2) {
            return false;
        // 2 is a Prime Number so if it is not 2 but is divisible by 2 it is not a Prime Number
        } else if (n % 2 == 0 && n != 2) {
            return false;
        } else {
            // Check every number from 3 up to the square root of the number(limit) to see if it is Prime
            // If a number does not have any factor till sqrt(n), it will not have any other factor
            for (int j = 3; j <= Math.sqrt(n); j = j + 2) {
                // If the number is divisible by any number other than itself and 1 it's not a Prime
                if (n % j == 0) {
                    return false;
                }
            }
            return true;
        }
    }
}

