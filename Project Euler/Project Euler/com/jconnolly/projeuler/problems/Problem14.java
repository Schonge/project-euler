package com.jconnolly.projeuler.problems;

/**
 * The following iterative sequence is defined for the set of positive integers:
 * n -> n/2 (n is even)
 * n -> 3n + 1 (n is odd)
 * 
 * Which starting number, under one million, produces the longest chain?
 * NOTE: Once the chain starts the terms are allowed to go above one million.
 * 
 * @author John Connolly 
 * @version v1.0
 * 
 */

public class Problem14
{
    public static void main(String[] args)
    {
        long number ;
        long n ;
        int chainSize ;
        int maxChain = 0 ;
        long maxNumber = 0 ;
        for(number = 10 ; number < 1000000 ; number++)
        {
            n = number ;
            // Counter to measure the size of the chain
            chainSize = 1 ;
            while(n > 1)
            {
                if(isEven(n))
                {
                    n /= 2 ;
                } else {
                    n = (3 * n) + 1 ;
                }
                chainSize++ ;
            }
            if(chainSize > maxChain)
            {
                maxChain = chainSize ;
                maxNumber = number ;
            }
        }
        System.out.println("Number is " + maxNumber + " and chain size is " + maxChain) ;        
    }
    
    // Method to check if number is even
    public static boolean isEven(long num)
    {
        if(num % 2 == 0)
        {
            return true ;
        } else {
            return false ;
        }
    }
}

// Correct Answer
