package com.jconnolly.projeuler.problems;

/**
 * A palindromic number reads the same both ways.
 * Find the largest palindrome made from the product of two 3-digit numbers.
 * 
 * @author (John Connolly) 
 * @version (v1.0)
 */

public class Problem4
{
    public static void main(String[] args)
    {
        int palindrome, maxPalindrome = 0 ;
        // Two for loops to make sure each 3 digit number is multiplied by each other
        for(int i = 100 ; i <= 999 ; i++) {
            for(int j = 101 ; j <= 999 ; j++) {
                palindrome = i * j ;
                if(isPalindrome(palindrome) == true && palindrome > maxPalindrome) {
                    maxPalindrome = palindrome ;
                }
            }
        }
        System.out.print(maxPalindrome) ;
    }
    
    // Check if number is a palindrome
    public static boolean isPalindrome(int number)
    {
        int original = number ;
        int reverse = 0 ;
        // Reverses the number to see if it is the same as the original(i.e. Palindrome)
        while(number > 0)
        {
            reverse = (reverse * 10) + (number % 10) ;
            // Gets the the remainder of the number to be reveresed
            number /= 10 ;
        }
         return reverse == original ;
    }
}

// Correct Answer
