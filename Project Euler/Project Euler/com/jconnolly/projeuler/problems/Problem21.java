package com.jconnolly.projeuler.problems;

/**
 * Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
 * If d(a) = b and d(b) = a, where a != b, then a and b are an amicable pair and each of a and b are called amicable numbers.
 *
 * For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
 *
 * Evaluate the sum of all the amicable numbers under 10000.
 * 
 * @author John Connolly
 * @version v1.0
 *
 */

public class Problem21 {
	
	public static void main(String[] args)
	{
		Problem21 p21 = new Problem21() ;
		p21.execute() ;
	}
	
	// This method runs the loop to check all numbers and adds up all the amicable ones
	public void execute()
	{
		int sum = 0 ;
		for(int i = 1 ; i <= 10000 ; i++)
		{
			if(isAmicable(i))
			{
				sum += i ;
			}
		}
		System.out.println(sum) ;
	}
	
	// This method checks if a number is amicable
	public boolean isAmicable(int num)
	{
		int j = getSumOfDivs(num) ;
		// Check to make sure a != b as suggested and that the divisors of b = a
		return j != num && getSumOfDivs(j) == num ;
		
	}
	
	// This method sums up the divisors of a number
	public int getSumOfDivs(int num)
	{
		int sum = 0 ;
		for(int i = 1 ; i < num ; i++)
		{
			if(num % i == 0)
			{
				sum += i ;
			}
		}
		return sum ;
	}
}
