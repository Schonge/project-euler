package com.jconnolly.projeuler.problems;

/**
 * By considering the terms in the Fibonacci sequence whose values do not exceed four million, 
 * find the sum of the even-valued terms.
 * 
 * @author John Connolly 
 * @version v1.0
 */

public class Problem2
{
    public static void main(String[] args)
    {
        int n1 = 0, n2 = 1 ;
        int n3 ;
        int sum = 0 ;
        while(n2 < 4000000) {
            n3 = n1 + n2 ;
            // Check to see if n3 is even
            if(n3 % 2 == 0)
            {
                // Sum up all the even numbers
                sum += n3 ;
            }
            n1 = n2 ;
            n2 = n3 ;
        }
        System.out.print(sum) ;
    }
}

// Correct Answer
