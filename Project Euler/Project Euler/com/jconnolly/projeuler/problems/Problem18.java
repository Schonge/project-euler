package com.jconnolly.projeuler.problems;

import java.util.Scanner ;
import java.io.InputStream;

/**
 * 
 * By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.
 * 		3
 * 	   7 4
 *    2 4 6
 *   8 5 9 3
 *   
 * That is, 3 + 7 + 4 + 9 = 23.
 * 
 * Find the maximum total from top to bottom of the triangle below:
 * 						   75
 * 						  95 64
 * 					     17 47 82
 * 					    18 35 87 10
 * 					   20 04 82 47 65
 * 					  19 01 23 75 03 34
 * 				     88 02 77 73 07 63 67
 * 				    99 65 04 28 06 16 70 92
 * 				   41 41 26 56 83 40 80 70 33
 * 				  41 48 72 33 47 32 37 16 94 29
 * 			     53 71 44 65 25 43 91 52 97 51 14
 * 			    70 11 33 28 77 73 17 78 39 68 17 57
 * 			   91 71 52 38 17 14 91 43 58 50 27 29 48
 * 			  63 66 04 68 89 53 67 30 73 16 69 87 40 31
 * 		     04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
 * 
 * NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route. However, Problem 67, is the same challenge with a triangle containing one-hundred rows; it cannot be solved by brute force, and requires a clever method! ;o)
 * 
 * @author John Connolly 
 * @version v1.0
 */

public class Problem18 {

	public static void main(String[] args) throws Exception
	{
		Problem18 p18 = new Problem18() ;
		p18.maxTotalPath() ;
	}
	
	public int[][] readFile() throws Exception
	{
		// Read in the file
		ClassLoader loader = Thread.currentThread().getContextClassLoader() ;
    	InputStream file = loader.getResourceAsStream("Triangle.txt") ;
    	
    	Scanner scan = new Scanner(file) ;
    	
    	// Create the array
    	int[][] triangle = new int[15][15] ;
    	String line ;
    	int m = 0, n = 0 ;
    	while(scan.hasNext())
    	{
    		// Convert each line into an array of numbers as strings
    		line = scan.nextLine() ;
    		String[] numbers = line.split(" ") ;

    	
    		// Convert the strings to integers
    		for(int i = 0 ; i < numbers.length ; i++)
            {
                triangle[m][n] = Integer.parseInt(numbers[i]) ;
                //System.out.print(triangle[m][n] + " ") ;
                n += 1 ;
            }
            n = 0 ;
            // System.out.println("") ;
            m += 1 ;
    	}
    	scan.close() ;
    	return triangle ;
	}
	
	/* 
	 * Algorithm to find max path:
	 * 
	 * Start at the line 1 up from the bottom.
	 * Take each number on that line and check the adjacent numbers to it on the line below.
	 * Take the largest of the 2 adjacent numbers and add it to the number at the node point on the line.
	 * Repeat this until you get to the apex of the triangle which will then represent the sum of the max path.
	 * 
	 */
	public void maxTotalPath() throws Exception
	{
		int[][] arr = readFile() ;
		for (int i = arr.length - 2 ; i >= 0 ; i--) 
		{
			for (int j = 0 ; j <= arr[i].length - 1 ; j++) 
			{
			    if (j != arr[i].length - 1) {
			        arr[i][j] += Math.max(arr[i + 1][j], arr[i + 1][j + 1]); 
			    } else {
			        arr[i][j] += arr[i + 1][j];
			    }
			}
		}
		System.out.println(arr[0][0]) ;
	}
}

// Correct Answer
