package com.jconnolly.projeuler.problems;

/**
 * Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order.
 * Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.
 * 
 * For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 � 53 = 49714.
 * 
 * What is the total of all the name scores in the file?
 * 
 * @author John Connolly
 * @version v1.0
 *
 */

import java.io.InputStream;
import java.util.Scanner ;
import java.util.Arrays ;

public class Problem22 {

	public static void main(String[] args)
	{
		Problem22 p22 = new Problem22() ;
		p22.execute() ;
	}
	
	public void execute()
	{
		// Load in file
		ClassLoader loader = Thread.currentThread().getContextClassLoader() ;
		InputStream file = loader.getResourceAsStream("Names.txt") ;
		
		Scanner scan = new Scanner(file) ;
		String[] list = new String[5200] ;
		String line ;
		while(scan.hasNext())
		{
			// Separate list of names into array of Strings
			line = scan.nextLine() ;
			list = line.split(",") ;
		}
		scan.close() ;
		sort(list) ;
		totalNameScores(list) ;
	}
	
	// Method for sorting array into alphabetical order
	public String[] sort(String[] array)
	{
		String[] orderedArr = array ;
		// Using sort() method from Arrays class to sort the array
		Arrays.sort(orderedArr);
		return orderedArr ;
	}
	
	// Method for calculating the scores of the names and totals
	public void totalNameScores(String[] arr)
	{
		int value = 0, count = 0 ;
		// Loop through array
		for(int i = 0 ; i < arr.length ; i++)
		{
			value = 0 ;
			// Loop through characters
			for(int j = 0 ; j < arr[i].length() ; j++)
			{
				// Checks to see if character is double quote(")
				// If it is then skip it using continue keyword
				if(arr[i].charAt(j) == 34)
				{
					continue ;
				}
				/* Subtract the ASCII value of A from the character value to get
				 * it's position in the alphabet which is also the letters score
				 * Need to add 1 in order to start at value 1 and not 0
				 */
				value += arr[i].charAt(j) - 'A' + 1 ;
				//System.out.println(value) ;
			}
			// Counts the total sum of all the names' scores
			count += (value * (i + 1)) ;
		}
		System.out.println(count) ;
	}
}

// Correct Answer
