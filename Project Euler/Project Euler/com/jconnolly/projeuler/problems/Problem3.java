package com.jconnolly.projeuler.problems;

/**
 * What is the largest prime factor of the number 600851475143 ?
 * 
 * @author John Connolly 
 * @version v1.0
 */

public class Problem3
{
    public static void main(String[] args)
    {
        long number = 600851475143l ;
        long maxfactor = 0 ;
        for(int i = 2 ; i <= Math.sqrt(number) ; i++)
        {
            // Check if i is a factor
            if(number % i == 0)
            {
                // Perform isPrime method on i to see if it is a prime number 
                if(isPrime(i))
                {
                    maxfactor = i ;
                }
            }
        }
        System.out.println(maxfactor) ;
    }
    
    // Method to check if number is prime
    public static boolean isPrime(long n) {
        // Both 0 and 1 are not Prime Numbers
        if (n < 2) {
            return false;
        // 2 is a Prime Number so if it is not 2 but is divisible by 2 it is not a Prime Number
        } else if (n % 2 == 0 && n != 2) {
            return false;
        } else {
            // Check every number from 3 up to the square root of the number(limit) to see if it is Prime
            // If a number does not have any factor till sqrt(n), it will not have any other factor
            for (int j = 3; j <= Math.sqrt(n); j = j + 2) {
                // If the number is divisible by any number other than itself and 1 it's not a Prime
                if (n % j == 0) {
                    return false;
                }
            }
            return true;
        }
    }
}

// Correct Answer
