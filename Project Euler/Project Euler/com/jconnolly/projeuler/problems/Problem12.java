package com.jconnolly.projeuler.problems;

/**
 * What is the value of the first triangle number to have over five hundred divisors?
 * 
 * @author John Connolly 
 * @version v1.0
 */

public class Problem12
{
    public static void main(String[] args)
    {
        int number = 0 ;
        // Iterate to a number large enough to be able to find 500 divisors
        for(int i = 1; i <= 500000 ; i++)
        {
            // number variable will hold the number that has the 500 divisors 
            number += i ;
            // Run the divisors method until it gets to 500 divisors
            if(divisors(number) > 500)
            {
                System.out.print(number) ;
                break ;
            }
        }
    }
        
    // Count the divisors of the number
    public static int divisors(int num)
    {
        int divisors = 0 ;
        // Check for the divisors(factors)
        for(int j = 1 ; j <= Math.sqrt(num) ; j++)
        {
            if(num % j == 0)
            {
                divisors++ ;
            }
        }
        /* By using Math.sqrt() you are only finding half of the factors
         This is used because if there is a factor before the sqrt there is one after it
         This is done as a performance enhancer so that you don't have to check for all numbers
         in a very large loop
         Be careful though for a count because you have to multiply by 2 to get the total factors */
        return 2 * divisors ;
    }
}

// Correct Answer
