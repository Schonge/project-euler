package com.jconnolly.projeuler.problems;

/**
 * n! means n × (n − 1) × ... × 3 × 2 × 1
 * 
 * For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
 * and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
 * 
 * Find the sum of the digits in the number 100!
 * 
 * @author John Connolly 
 * @version v1.0
 */

import java.math.BigInteger ;

public class Problem20
{
    public static void main(String[] args)
    {
        // Use BigInteger as numbers are too large
        BigInteger number = BigInteger.valueOf(100) ;
        // Gets factorial of number
        BigInteger result = factorial(number) ;
        //Converts the reult of the factorial to a string
        String s = result.toString() ;
        BigInteger sum = BigInteger.ZERO ;
        // Iterates through the string getting the individual digits and adding them together
        for(int i = 0 ; i < s.length() ; i++)
        {
            char c = s.charAt(i) ;
            sum = sum.add(BigInteger.valueOf(Character.getNumericValue(c))) ;
        }
        System.out.println(sum) ;
    }
    
    // Method for getting factorial - uses recursion
    public static BigInteger factorial(BigInteger n)
    {
        if(n.compareTo(BigInteger.ZERO)==0)
        {
            return BigInteger.valueOf(1) ;
        } else {
            return n.multiply(factorial(n.subtract(BigInteger.valueOf(1)))) ;
        }
    }
}

// Correct Answer

