package com.jconnolly.projeuler.problems;

/**
 * Starting in the top left corner of a 2x2 grid, and only being able to move to the right and down,
 * there are exactly 6 routes to the bottom right corner.
 * 
 * How many such routes are there through a 20x20 grid?
 * 
 * @author John Connolly 
 * @version v1.0
 */

import java.math.BigInteger ;

public class Problem15
{
    public static void main(String[] args)
    {
        /*
         * Use combinations formula nCr
         * 40 is the least amount of moves that are taken. 20 is the least amount of right moves.
         * n = 40, r = 20
         * Formula = n! / r! * (n-r)!
        */
        BigInteger moves = BigInteger.ZERO ;
        BigInteger n = BigInteger.valueOf(40) ;
        BigInteger r = BigInteger.valueOf(20) ;
        /*
         * System.out.println(factorial(n)) ;
         * System.out.println(factorial(r)) ;
         * System.out.println(factorial(n.subtract(r))) ;
         * System.out.println(factorial(r).multiply(factorial(n.subtract(r)))) ;
        */
        moves = factorial(n).divide((factorial(r).multiply(factorial(n.subtract(r))))) ;
        System.out.println(moves) ;
    }
    
    // Factorial formula for recursive use
    public static BigInteger factorial(BigInteger num)
    {
        if(num.compareTo(BigInteger.ZERO)==0)
        {
            return BigInteger.valueOf(1) ;
        } else {
            return num.multiply(factorial(num.subtract(BigInteger.valueOf(1)))) ;
        }
    }
}

// Correct Answer

