package com.jconnolly.projeuler.problems;

/**
 * Write a description of class Problem8 here.
 * 
 * Find the greatest product of five consecutive digits in the 1000-digit number.
 */

import java.util.Scanner ;
import java.io.InputStream;

public class Problem8
{
    public static void main(String[] args) throws Exception
    {
        /* 
         * Created a new File to hold the large number instead of having it hard coded in the
         * solution. This also allowed me to build up my skills working with and reading in files.
        */
    	ClassLoader loader = Thread.currentThread().getContextClassLoader() ;
    	InputStream file = loader.getResourceAsStream("ThousandDigitNumber.txt") ;
        // Used Scanner to read in the file
        Scanner scan = new Scanner(file) ;
        int greatestProd = 0, i = 0 ;
        // Read in the number as a String
        String number = scan.next() ;
        /* 
         * String length has to be length() - 5 otherwise there will be a IndexOutOfBounds error
         * The check is for the product of the next 5 numbers so it cant have less than five therefore
         * it must stop looping 5 charactes from the end of the String
        */
        while(i < number.length() - 5)
        {
            // Call the productOfNumsMethod to get the product of the 5 consecutive digits
            int product = productOfNums(number, i) ;
            // If the returned product is greater than the product before it becomes the greatest product
            if(product > greatestProd)
            {
                greatestProd = product ;
            }
            i++ ;
        }
        System.out.println(greatestProd) ;
        // It is good practice to close the stream when done
        scan.close() ;
    }
    
    // Method for calculating product
    public static int productOfNums(String s, int num)
    {
        int multiplication = 1 ;
        // Loop through the 5 consecutive digits
        for(int j = num ; j < num + 5 ; j++)
        {
            // Can't manipulate String so must convert to characters
            char c = s.charAt(j) ;
            // Get the numeric value of the character to be used in the multiplication process
            multiplication *= Character.getNumericValue(c) ;
        }
        return multiplication ;
    }
}

// Correct Answer
