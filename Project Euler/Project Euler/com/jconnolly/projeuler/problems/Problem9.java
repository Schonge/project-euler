package com.jconnolly.projeuler.problems;

/**
 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which, a2 + b2 = c2
 * For example, 32 + 42 = 9 + 16 = 25 = 52.
 * 
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 * Find the product abc.
 * 
 * @author John Connolly 
 * @version v1.0
 */

public class Problem9
{
    public static void main(String[] args)
    {
        // Loop through all the possibilities of the 3 integers
        for(int a = 1 ; a < 1000 ; a++)
        {
            for(int b = a + 1 ; b < 1000 ; b++)
            {
                for(int c = b + 1 ; c < 1000 ; c++)
                {
                    // Check if a2 + b2 = c2 AND a + b + c = 1000
                    if(Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2) && (a + b + c) == 1000)
                    {
                        // Print out the product of a, b, c
                        System.out.println(a * b * c) ;
                        break ;
                    }
                }
            }
        }
    }
    
}

// Correct Answer
