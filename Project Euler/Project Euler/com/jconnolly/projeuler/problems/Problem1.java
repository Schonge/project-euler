package com.jconnolly.projeuler.problems;

/**
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
 * Find the sum of all the multiples of 3 or 5 below 1000.
 * 
 * @author John Connolly 
 * @version v1.0
 */

public class Problem1
{
    public static void main(String[] args)
    {
        int sum = 0 ;
        // Loop through natural numbers
        for(int i = 1 ; i < 1000 ; i++) {
            // Check if number has multiple of 3 or 5
            if((i % 3 == 0) || (i % 5 == 0))
            {
                // If it has multiple of 3 or 5 add the number to sum
                // This gets total of the sum of all the numbers with multiples 3 or 5
                sum = sum + i ;
            }
        }
        System.out.print(sum) ;
    }
}

// Correct Answer
